# @universis/common
[Universis](https://universis.gr) is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

**@universis/common** package contains common components and services for building client applications for Universis project.

### Usage

    npm i @universis/common

### Development

Import `@universis/common` as submodule in any angular cli project by replacing `newProjectRoot` as already configured in your `angular.json`

    git submodule add https://gitlab.com/universis/common.git <newProjectRoot>/common

e.g.

    git submodule add https://gitlab.com/universis/common.git projects/common

Add the following entries to `tsconfig.app.json#compilerOptions.paths`:

    {
        "compilerOptions": {
            "paths": {
                "@universis/common/routing": [
                    "<newProjectRoot>/common/routing/src/public_api"
                ],
                "@universis/common/testing": [
                    "<newProjectRoot>/common/testing/src/public_api"
                ],
                "@universis/common": [
                    "<newProjectRoot>/common/src/public_api"
                ]
                ...
            }
        }
    }  

If you want to include `@universis/common` as an angular cli project, include the following section in `angular.json` under `projects`:

    "projects": {
      "common": {
        "root": "<newProjectRoot>/common",
        "sourceRoot": "<newProjectRoot>/common/src",
        "projectType": "library",
        "prefix": "lib",
        "architect": {
          "build": {
            "builder": "@angular-devkit/build-ng-packagr:build",
            "options": {
              "tsConfig": "<newProjectRoot>/common/tsconfig.lib.json",
              "project": "<newProjectRoot>/common/ng-package.json"
            }
          },
          "test": {
            "builder": "@angular-devkit/build-angular:karma",
            "options": {
              "main": "<newProjectRoot>/common/src/test.ts",
              "polyfills": "<newProjectRoot>/common/src/polyfills.ts",
              "tsConfig": "<newProjectRoot>/common/tsconfig.spec.json",
              "karmaConfig": "<newProjectRoot>/common/karma.conf.js",
              "watch": false,
              "codeCoverage": true,
              "scripts": []
            }
          },
          "lint": {
            "builder": "@angular-devkit/build-angular:tslint",
            "options": {
              "tsConfig": [
                "<newProjectRoot>/common/tsconfig.lib.json",
                "<newProjectRoot>/common/tsconfig.spec.json"
              ],
              "exclude": [
                "**/node_modules/**"
              ]
            }
          }
        }
      },
      ...
    }

### Testing

#### Intro
In order to test **@universis/common** package, we use Karma as our test-runner and Jasmine as our 
testing framework. We also use **Puppeteer**, a high-level API to manipulate the dev-tools of any 
chromium based browser. You should keep in mind that puppeteer is a peer dependency of the whole project
and should be placed in the parent directory of the repository `universis`. 

#### Example

To run all of our tests simply run

```
ng test common
```

This will compile the common package and run our test suite in a `ChromeHeadless` browser with no sandbox.

#### Extra

You can also have a visual representation of the tests running by adding
```
      ChromeNoSandbox: {
        base: 'Chrome',
        flags: [
          '--no-sandbox',
          '--enable-logging=stderr',
          '--disable-web-security',
          '--disable-gpu',
          '--no-proxy-server'
        ]
      },

```
under the `customLaunchers` in `packages/common/karma.conf.js` 
