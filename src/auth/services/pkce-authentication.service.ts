import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ConfigurationService } from '../../shared/services/configuration.service';
import { ActivatedUser } from './activated-user.service';
import { AuthCallbackResponse, AuthenticationService, OnRefreshToken } from './authentication.service';
import { SHA256, enc } from 'crypto-js';
import { HttpClient } from '@angular/common/http';
import { ResponseError } from '@themost/client';
import { first } from 'rxjs/operators';

function base64URLEncode(str: string) {
    return str.replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/=/g, '');
}

declare interface ExchangeCodeResponse {
    access_token: string;
    token_type: string;
    expires_in: number;
    refresh_token: string;
    scope?: string;
}

@Injectable()
export class PkceAuthenticationService extends AuthenticationService implements OnRefreshToken {

    constructor(private http: HttpClient,
                configuration: ConfigurationService,
                context: AngularDataContext,
                activatedRoute: ActivatedRoute,
                activatedUser: ActivatedUser) {
        super(configuration, context, activatedRoute, activatedUser);
    }
    /**
     * Refreshes OAuth2 access token
     * @returns {AuthCallbackResponse=}
     */
    refresh(): Promise<AuthCallbackResponse | void> {
        return new Promise((resolve, reject) => {
            // get activated user
            this.activatedUser.user.pipe(first()).subscribe((user) => {
                // if user exists
                if (user && user.token && user.token.refresh_token) {
                    // get token url
                    const oauth2Settings: {
                        clientID?: string;
                        tokenURL?: string
                    } = this.configuration.settings.auth &&
                        this.configuration.settings.auth.oauth2;
                    if (oauth2Settings && oauth2Settings.tokenURL) {
                        const form = new URLSearchParams();
                        form.set('grant_type', 'refresh_token');
                        form.set('client_id', oauth2Settings.clientID);
                        form.set('refresh_token', user.token.refresh_token);
                        return this.http.post<AuthCallbackResponse>(
                            oauth2Settings.tokenURL,
                            form.toString(),
                            {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            }
                        ).subscribe((result) => {
                            return resolve(result);
                        }, (err) => {
                            return reject(err);
                        });
                    } else {
                        return reject(new Error('Invalid application configuration. Token URL is missing.'));
                    }
                }
                return resolve();
            }, (err) => {
                return reject(err);
            });
        });
    }

    /**
     * Authorize user by using PKCE Authorization Flow
     */
    authorize() {
        const settings = this.configuration.settings.auth;
        /* tslint:disable-next-line max-line-length */
        // get scopes
        settings.oauth2.scope = settings.oauth2.scope || [];
        // get query params
        const queryParams = this.activatedRoute.snapshot.queryParams;
        // get current code_verifier
        const code_verifier = this.getCodeVerifier();
        // get continue param or use /
        let continueParam = '/';
        if (queryParams.continue && /^\//.test(queryParams.continue)) {
            continueParam = queryParams.continue;
        }
        const state = this.generateState(continueParam);
        // get pkce code challenge
        const code_challenge = SHA256(code_verifier).toString(enc.Base64);
        // and method
        const code_challenge_method = 'S256';
        // redirect client
        window.location.href = `${settings.authorizeURL}?` +
            `redirect_uri=${encodeURIComponent(settings.oauth2.callbackURL)}` +
            `&response_type=code` +
            `&client_id=${settings.oauth2.clientID}` +
            `&code_challenge=${base64URLEncode(code_challenge)}` +
            `&code_challenge_method=${code_challenge_method}` +
            `&scope=${settings.oauth2.scope.join(',')}` +
            `&state=${state}`;
      }

      /**
       * H
       * @param {*} queryParams
       */
      async callback(queryParams: any): Promise<any> {
          this.preCallback(queryParams);
          // if callback contains access_token
        if (Object.prototype.hasOwnProperty.call(queryParams, 'access_token')) {
            // handle access token and continue
            return super.callback(queryParams);
        }
        if (Object.prototype.hasOwnProperty.call(queryParams, 'code')) {
            // handle code exchange
            const oauth2Settings: {
                clientID?: string,
                tokenURL?: string,
                callbackURL: string
            } = this.configuration.settings.auth &&
                this.configuration.settings.auth.oauth2;
            if (oauth2Settings == null) {
                throw new Error('Application configuration is invalid. OAuth2 configuration section is missing.');
            }
            if (oauth2Settings.tokenURL == null) {
                throw new Error('OAuth2 configuration section is invalid. Code exchange URL is missing.');
            }

            // get current code verifier
            const code_verifier = this.getCodeVerifier();
            // prepare form
            const form = new URLSearchParams();
            form.set('grant_type', 'authorization_code');
            form.set('code', queryParams.code);
            form.set('client_id', oauth2Settings.clientID);
            form.set('redirect_uri', oauth2Settings.callbackURL);
            form.set('code_verifier', code_verifier);
            try {
                const response = await this.http.post<ExchangeCodeResponse>(oauth2Settings.tokenURL, form.toString(), {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
               }).toPromise();
               return super.callback(response);
            } catch (err) {
                console.error('An error occurred while exchange code.');
                if (err.status === 0) {
                    throw new ResponseError('Connection Failed', 0.1);
                }
                throw err;
            }
        }
        // otherwise, throw error if access_token or code is empty
        throw new ResponseError('Bad authorization callback request', 400);
      }
}
