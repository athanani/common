import {
  Component,
  ComponentFactoryResolver,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { UserService } from './services/user.service';
import {ErrorService} from '../error/error.service';
import { AES, enc } from 'crypto-js';
import { Subscription } from 'rxjs';

@Component({
  selector: 'universis-auth-callback',
  template: '<div></div>',
  encapsulation: ViewEncapsulation.None,
})

export class AuthCallbackComponent implements OnInit, OnDestroy {
  private queryMapSubscription: Subscription;

  constructor(
    private _router: Router,
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _errorService: ErrorService,
    private _authService: AuthenticationService) {
  }
  ngOnDestroy(): void {
    if (this.queryMapSubscription) {
      this.queryMapSubscription.unsubscribe();
    }
  }

  ngOnInit() {
    // validate code

    this.queryMapSubscription = this._route.queryParamMap.subscribe((paramMap: ParamMap) => {
      const params: any = paramMap.keys.reduce((obj, currentValue, currentIndex) => {
        const values = paramMap.getAll(currentValue);
        Object.defineProperty(obj, currentValue, {
          enumerable: true,
          configurable: true,
          writable: true,
          value: values.length > 1 ? values : values[0]
        });
        return obj;
      }, {});
      this._authService.callback(params).then((res) => {

        if (typeof res === 'object') {
          // try to get state parameter
          let navigateURL = '/';

          const state = paramMap.get('state');
          // get code verifier
          if (state && res.token && res.token.code_verifier) {
            // try to decrypt state parameter
            try {
              // get hex bytes
              const hex = enc.Hex.parse(state);
              // convert hex to base64
              const b64 = hex.toString(enc.Base64);
              // decrypt state parameter
              const stateURL = AES.decrypt(b64, res.token.code_verifier).toString(enc.Utf8);
              // if stateURL is a relative URL
              if (stateURL && /^\//.test(stateURL)) {
                // set navigate to state URL
                navigateURL = stateURL;
              }
            } catch (err) {
              console.error('AUTH', 'STATE', err);
            }
          }
          // and finally navigate client
          return this._router.navigate([navigateURL]);
        }
      }).catch( err => {
        return this._errorService.navigateToError(err);
      });
    });
  }

}
