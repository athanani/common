import { ErrorHandler, Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
@Injectable()
export class ErrorsHandler implements ErrorHandler {

  static navigateToError(status: number, continueLink?: string): void {
    if (continueLink) {
      window.location.href = `#/error/${status}?continue=${encodeURIComponent(continueLink)}`;
    } else {
      window.location.href = `#/error/${status}`;
    }
  }



  handleError(error: any) {
      // handle http response errors
      if (error.rejection && typeof error.rejection.status === 'number') {
          switch(error.rejection.status) {
              case 0:
                return ErrorsHandler.navigateToError(408.1);
              case 401:
              case 498:
                return ErrorsHandler.navigateToError(401.1, '/auth/loginAs');
              case 403.3:
              case 408:
              case 429:
              case 502:
              case 503:
                return ErrorsHandler.navigateToError(error.rejection.status);
          }
      }
  }



}
