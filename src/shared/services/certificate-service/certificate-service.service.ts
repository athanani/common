import { Injectable } from '@angular/core';
import { Args } from '@themost/client';
import { X509 } from 'jsrsasign';

@Injectable({
  providedIn: 'root'
})
export class CertificateService {

  constructor() { }

  /**
   *
   * It returns a X509 certificate from the raw certificate sting
   *
   * @param {string} certificate The raw user certificate
   *
   * @returns {X509} The certificate object in x509 format
   *
   */
  getX509Certificate(certificate) {
    const formatted = this.formatCertificate(certificate);
    const x509 = new X509();
    x509.readCertPEM(formatted);
    return x509;
  }

  /**
   *
   * Parses a certificate from a continuous string of chars in a PEM formatted
   * certificate
   *
   * @param {string} certificate The x509 certificate in raw format
   *
   * @returns {X509} The certificate object in x509 format
   *
   */
  formatCertificate(certificate: string): X509 {
    Args.notNull(certificate, 'Certificate must be defined');
    Args.notEmpty(certificate, 'Certificate can not be empty');

    const certHead = `-----BEGIN CERTIFICATE-----`;
    const certTail = `-----END CERTIFICATE-----`;
    const splittedCert = this.splitStringToChunks(certificate, 64);
    const parts = [certHead, ...splittedCert,  certTail];
    return parts.join('\n');
  }

  /**
   *
   * Splits a string of text in an array of same length characters
   *
   * @param {string} payload The string to be split
   * @param {number} lineLength The number  of characters in line
   *
   * @returns {Array<string>} The split string parts
   *
   */
  splitStringToChunks(payload: string, lineLength: number): string[] {
    Args.notNull(payload, 'Certificate must be defined');
    Args.notEmpty(payload, 'Certificate can not be empty');
    if (!lineLength || lineLength <= 0) {
      throw new Error('Line length can not be negative or zero');
    }

    let index = 0;
    let remainingChars = payload.length;
    let maximumIteration = payload.length + 1;

    const parts: string[] = [];
    // split the certificate in 64-character length lines
    while(remainingChars > 0) {

      if (index > maximumIteration) {
        throw new Error('Maximum number of  iterations exceeded');
      }

      parts.push(payload.substring(index*lineLength, (index+1)*lineLength));
      remainingChars = payload.length - (index + 1) * lineLength;
      index++;
    }

    return parts;
  }

  /**
   *
   * Parses a jsrsasign to a Date object
   *
   * @param {string} date The date as is returned form jsrsasign library in UTC 0
   *
   * @returns {Date} The parsed date
   */
  parseCertificateDate(date: string): Date {
    const parts = date.match(/.{1,2}/g); //split the date to 2-char wide parts
    const yearPrefix = new Date().getFullYear().toString().substring(0,2); // The current millennium
    parts[0] = yearPrefix + parts[0];

    // construct a js-friendly date string
    const asString = `${parts[0]}-${parts[1]}-${parts[2]} ${parts[3]}:${parts[4]}:${parts[5]}.000Z`;
    return new Date(asString);
  }

  /**
   * Gets certificate extensions attributes
   *
   */
  private static getCertificateParams(certificate: X509): any[] {
    const extension = certificate.parseExt();
    if(extension !== -1 && Array.isArray(certificate.aExtInfo)){
      return certificate.getExtParamArray();
    }
    return [];
  }

  /**
   * Extracts key usages from the certificate:
   * It parses X509 v3 key and extended key usages
   * and returns an array with the purposes.
   * @param {X509} certificate
   */
  extractPurposes(certificate:X509): Array<any>{
    const params = CertificateService.getCertificateParams(certificate);
    if ((params.filter(x => x.extname === 'extKeyUsage')).length > 0){
      const commonOIDs = CertificateService.mapOIDToString();
      let purposes =certificate.getExtExtKeyUsage();
      if(purposes && purposes.array){
        purposes= purposes.array;
      }
      let keyUsage = params.filter(x => x.extname === 'keyUsage');
      purposes = [...purposes,  ...keyUsage[0].names];
      purposes = purposes.map(purpose => {
        if(commonOIDs.has(purpose)){
          purpose = commonOIDs.get(purpose);
        }
        return purpose;
      });
      return Array.from(new Set(purposes));
    }
  }

  /**
   * Creates a map of common keyUsage OIDs
   * to their name
   * @return {Map<string,string>}
   */
  private static mapOIDToString(){
    const commonOIDs = new Map<string, string>();
    // Any OID starting with 1.3.6.1.5.5.7.3 is
    // directly defined in x509 v3 req key purposes
    commonOIDs.set("1.3.6.1.5.5.7.3.1", "serverAuth");
    commonOIDs.set("1.3.6.1.5.5.7.3.2", "clientAuth")
    commonOIDs.set("1.3.6.1.5.5.7.3.3", "codeSigning");
    commonOIDs.set("1.3.6.1.5.5.7.3.4", "emailProtection")
    commonOIDs.set("1.3.6.1.5.5.7.3.8","timestamping");
    // Any OID starting with 1.3.6.1.4.1.311
    // is provided by Microsoft
    commonOIDs.set("1.3.6.1.4.1.311.20.2.2", "smartCardLogon");
    commonOIDs.set("1.3.6.1.4.1.311.10.3.12", "documentSign");
    commonOIDs.set("1.3.6.1.4.1.311.80.1", "documentEnc");
    commonOIDs.set("2.5.29.37.0", "any");
    return commonOIDs;
  }

  /**
   * Extract the owner of the certificate:
   * In an X509 certificate the subject contains
   * information on the user and the common name
   * is the name of the user that the certificate
   * was issued to by the certificate authority.
   * @param {X509} certificate
   */
  extractCertificateOwner(certificate: X509) {
    const subjectCN = (certificate.getSubjectString())
      .split('/')
      .filter(x => x.includes('CN'))
      .join(',')
      .split('=')[1];
    let fullName = subjectCN.split(" ");
    if (fullName.length === 1){
      fullName = [...fullName, ""]
    }
    return {
      givenName: fullName[0],
      familyName: fullName[1]
    }
  }
}
