import {Injectable} from '@angular/core';
import {AsyncSubject, BehaviorSubject, Observable} from 'rxjs';
import { skip } from 'rxjs/operators';

@Injectable()
export class AppEventService {
    public readonly change: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    public readonly add: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    public readonly remove: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    public readonly added: Observable<any> = this.add.pipe(skip(1));
    public readonly changed: Observable<any> = this.change.pipe(skip(1));
    public readonly removed: Observable<any> = this.remove.pipe(skip(1));
    constructor() {
    }

}
