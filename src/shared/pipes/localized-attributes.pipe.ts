import {Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Pipe({
  name: 'localize',
  pure: true
})
export class LocalizedAttributesPipe implements PipeTransform {
  constructor(private translateService: TranslateService) {
  }

  /**
   * Display correct locales for models.
    @param {object} model
    @param {string} attribute
   */
  transform(model: object, attribute: string): any {
    if (model['locale'] && model['locale']['inLanguage'] == this.translateService.currentLang){
        if (model['locale'][attribute]) {
            return model['locale'][attribute]
        }
    }
    return model[attribute] || undefined
  }
}
