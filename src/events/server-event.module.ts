import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule, Type } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthModule } from '../auth/auth.module';
import { SharedModule } from '../shared/shared.module';
// tslint:disable-next-line: max-line-length
import { ServerEventService, ServerEventSubscriber, SERVER_EVENT_CHILD_SUBSCRIBERS, SERVER_EVENT_SUBSCRIBERS } from './services/server-event.service';

declare type EventSubscribers = Array<Type<ServerEventSubscriber>>;

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        RouterModule,
        SharedModule,
        AuthModule
    ]
})
export class ServerEventModule {
    static forRoot(subscribers?: EventSubscribers): ModuleWithProviders<ServerEventModule> {
        return {
            ngModule: ServerEventModule,
            providers: [
                ServerEventService,
                {
                    provide: SERVER_EVENT_SUBSCRIBERS,
                    useValue: subscribers || []
                },
                {
                    provide: SERVER_EVENT_CHILD_SUBSCRIBERS,
                    multi: true,
                    useValue: []
                }
            ]
        };
    }

    static forChild(subscribers: EventSubscribers): ModuleWithProviders<ServerEventModule> {
        return {
            ngModule: ServerEventModule,
            providers: [
                {
                    provide: SERVER_EVENT_CHILD_SUBSCRIBERS,
                    multi: true,
                    useValue: subscribers
                },
            ]
        };
    }

}
